<?php

use yii\db\Migration;

/**
 * Class m190428_180602_pages
 */
class m190428_180602_pages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'slug' => $this->string()->notNull()->unique(),
            'content' => $this->text()->notNull(),
            'type_id' =>  $this->integer()->notNull(),
            'created_at' => $this->timestamp()
        ]);
        $this->createIndex(
            'idx-pages-type_id',
            'pages',
            'type_id'
        );
	    $this->addForeignKey(
            'fk-pages-type_id',
            'pages',
            'type_id',
            'type',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'idx-pages-type_id',
            'pages'
        );


        $this->dropIndex(
            'fk-pages-type_id',
            'pages'
        );

        $this->dropTable('news');
    }


}
