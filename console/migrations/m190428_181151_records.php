<?php

use yii\db\Migration;

/**
 * Class m190428_181151_records
 */
class m190428_181151_records extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('records', [
            'id' => $this->primaryKey(),
            'full_name' => $this->string(50)->notNull(),
            'email' => $this->string(50)->notNull(),
            'date_record' =>  $this->timestamp()->notNull(),
            'comment' => $this->string(500),
            'type' => $this->tinyInteger()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('records');
    }

}
