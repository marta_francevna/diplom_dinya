<?php

namespace console\controllers;

use common\models\Type;
use yii\console\Controller;

class TypeController extends Controller
{
	public function actionIndex()
	{

	    $type = new Type();
        $type->title = 'admin_procedure';
        $type->name = 'Административные процедуры';
        $type->save();

        $type = new Type();
        $type->title = 'our_services';
        $type->name = 'Наши услуги';
        $type->save();

        $type = new Type();
        $type->title = 'appeal_of_citizens';
        $type->name = 'Обращение граждан';
        $type->save();

        $type = new Type();
        $type->title = 'about';
        $type->name = 'О нас';
        $type->save();

        $type = new Type();
        $type->title = 'legal_requirements';
        $type->name = 'Требования законодательства';
        $type->save();

    }
}