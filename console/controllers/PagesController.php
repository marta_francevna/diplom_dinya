<?php

namespace console\controllers;

use common\models\Pages;
use Faker\Factory;
use yii\console\Controller;

class PagesController extends Controller
{
	public function actionIndex()
	{
        $faker = Factory::create();

	    $pages = new Pages();
	    $pages->title = 'По заявлениям физических лиц';
        $pages->content = $faker->text(700);
        $pages->type_id = 1;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'По заявлениям юридических лиц и индивидуальных предпринимателей';
        $pages->content =  $faker->text(700);
        $pages->type_id = 1;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'По заявлениям физических лиц';
        $pages->content =  $faker->text(700);
        $pages->type_id = 1;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Система управления потоками клиентов';
        $pages->content =  $faker->text(700);
        $pages->type_id = 1;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Удостоверение сделок с обЪектами, расположенными в Витебской области';
        $pages->content =  $faker->text(700);
        $pages->type_id = 2;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Топографическая, контрольно-испонительная съемка';
        $pages->content =  $faker->text(700);
        $pages->type_id = 2;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Судебно-экспертная деятельность';
        $pages->content =  $faker->text(700);
        $pages->type_id = 2;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Риэлторские услуги';
        $pages->content =  $faker->text(700);
        $pages->type_id = 2;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Государственная регистрация недвижимого имущества';
        $pages->content =  $faker->text(700);
        $pages->type_id = 2;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Техническая инвентаризация недвижимого имущества';
        $pages->content =  $faker->text(700);
        $pages->type_id = 2;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Оценка капитальных строений, зданий, сооружений, изолированных помещений, машин, не завершенных строительством объектов, транспортных средств';
        $pages->content =  $faker->text(700);
        $pages->type_id = 2;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Система управления потоками клиентов';
        $pages->content =  $faker->text(700);
        $pages->type_id = 2;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Электронные обращения';
        $pages->content =  $faker->text(700);
        $pages->type_id = 3;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Электронные обращения граждан';
        $pages->content =  $faker->text(700);
        $pages->type_id = 3;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Электронные обращения юридических лиц';
        $pages->content =  $faker->text(700);
        $pages->type_id = 3;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Управление и отделы';
        $pages->content =  $faker->text(700);
        $pages->type_id = 4;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Структура агенства';
        $pages->content =  $faker->text(700);
        $pages->type_id = 4;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Реквизиты';
        $pages->content =  $faker->text(700);
        $pages->type_id = 4;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Филиалы и бюро';
        $pages->content =  $faker->text(700);
        $pages->type_id = 4;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Режим работы';
        $pages->content = $faker->text(700);
        $pages->type_id = 4;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Техническая инвентаризация';
        $pages->content =  $faker->text(700);
        $pages->type_id = 5;
        $pages->save();

        $pages = new Pages();
        $pages->title = 'Форма документов';
        $pages->content =  $faker->text(700);
        $pages->type_id = 5;
        $pages->save();
    }
}