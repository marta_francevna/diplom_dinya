<?php

namespace console\controllers;

use common\models\User;
use yii\console\Controller;

class UserController extends Controller
{
	public function actionIndex()
	{

	    $user = new User();
        $user->username = 'admin@gmail.com';
        $user->email = 'admin@gmail.com';
        $user->setPassword('admin');
        $user->generateAuthKey();
        $user->status = 10;
        $user->save();


    }
}