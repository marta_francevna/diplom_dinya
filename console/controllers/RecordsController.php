<?php

namespace console\controllers;

use common\models\News;
use common\models\Records;
use common\models\User;
use Faker\Factory;
use yii\console\Controller;

class RecordsController extends Controller
{
    public function actionIndex()
    {
        $faker = Factory::create();

        for ($i = 0; $i <= 30; $i++) {
            $records = new Records();
            $records->full_name = $faker->name;
            $records->email = $faker->email;
            $records->date_record = $faker->date();
            $records->comment = $faker->text(150);
            $records->type = rand(1, 2);
            $records->save();
        }

    }
}