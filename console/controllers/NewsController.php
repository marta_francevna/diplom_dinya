<?php

namespace console\controllers;

use common\models\News;
use common\models\User;
use Faker\Factory;
use yii\console\Controller;

class NewsController extends Controller
{
    public function actionIndex()
    {
        $faker = Factory::create();

        for ($i = 0; $i <= 30; $i++) {
            $pages = new News();
            $pages->title = $faker->text(20);
            $pages->content = $faker->text(700);
            $pages->save();
        }

    }
}