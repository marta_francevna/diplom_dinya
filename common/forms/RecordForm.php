<?php
namespace common\forms;

class RecordForm extends \yii\base\Model
{
    public $full_name;
	public $email;
	public $comment;
	public $date_record;
    public $type;

	public function rules()
	{
		return [
            [['full_name', 'email', 'date_record', 'type'], 'required'],
            [['full_name', 'email'], 'string', 'max' => 50],
            [['comment'], 'string', 'max' => 500],
			['email', 'email'],
		];
	}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'full_name' => 'ФИО',
            'type' => 'Тип',
            'date_record' => 'Дата записи',
            'email' => 'E-mail',
            'comment' => 'Комментарий',
        ];
    }
}