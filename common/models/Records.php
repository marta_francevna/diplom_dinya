<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "records".
 *
 * @property int $id
 * @property string $full_name
 * @property string $email
 * @property string $date_record
 * @property string $comment
 * @property int $type
 */
class Records extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'records';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['full_name', 'email', 'comment', 'type'], 'required'],
            [['date_record'], 'safe'],
            [['type'], 'integer'],
            [['full_name', 'email'], 'string', 'max' => 50],
            [['comment'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'ФИО',
            'email' => 'Email',
            'date_record' => 'Дата записи',
            'comment' => 'Комментарий',
            'type' => 'Тип',
        ];
    }
}
