<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id'                  => 'app-backend',
    'basePath'            => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'modules'             => [
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
    ],

    'controllerMap' => [
        'elfinder' => [
            'class'  => 'mihaildev\elfinder\PathController',
            'access' => ['@'],
            'root'   => [
                'path' => 'files',
                'name' => 'Files',
            ],
        ],
    ],
    'bootstrap'     => ['log'],
    'components'    => [
        'request'            => [
            'csrfParam' => '_csrf-backend',
        ],
        'db'                 => [
            'class'    => 'yii\db\Connection',
            'dsn'      => 'mysql:host=localhost;dbname=diplom',
            'username' => 'root',
            'password' => '2711',
            'charset'  => 'utf8',
        ],
        'user'               => [
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie'  => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session'            => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log'                => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler'       => [
            'errorAction' => 'site/error',
        ],
        'urlManager'         => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
                '/' => 'admin/index',
            ],
        ],
        'urlManagerFrontEnd' => [
            'class'           => 'yii\web\urlManager',
            'baseUrl'         => $params['frontendUrl'],
            'enablePrettyUrl' => false,
            'showScriptName'  => false,
        ],
    ],
    'params'        => $params,
];
