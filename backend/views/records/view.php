<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Records */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Запись', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="records-view">

    <h1><?= 'Запись'?></h1>

    <p>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'ФИО',
                'attribute' => 'full_name',

            ],
            'email:email',
            [
                'label' => 'Дата записи',
                'attribute' => 'date_record',
                'value' => function ($model) {

                    return Yii::$app->formatter->asDateTime($model->date_record, 'php:Y-m-d');

                },
            ],
            [
                'label' => 'Комментарий',
                'attribute' => 'comment',

            ],
            [
                'label' => 'Тип',
                'attribute' => 'type',
                'value' => function ($model) {
                    return $model->type == 1 ? 'Физ. лицо' : 'Юр. лицо';
                },

            ],
        ],
    ]) ?>

</div>
