<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\RecordsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Записи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="records-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style' => 'width: 100%; max-width: 100%'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => 'ФИО',
                'attribute' => 'full_name',

            ],
            'email:email',
            [
                'label' => 'Дата записи',
                'attribute' => 'date_record',
                'filter' => \yii\jui\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_record',
                    'dateFormat' => 'php:Y-m-d',
                    'options' => [
                        'class' => 'form-control',
                    ],
                ]),
                'value' => function ($model) {

                    return Yii::$app->formatter->asDateTime($model->date_record, 'php:Y-m-d');

                },

            ],
            [
                'label' => 'Комментарий',
                'attribute' => 'comment',

            ],
            [
                'label' => 'Тип',
                'attribute' => 'type_id',
                'value' => function ($data) {
                    return $data->type == 1 ? 'Физ. лицо' : 'Юр. лицо';
                },

            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'template' => '{view}{delete}',

            ]
        ],
    ]); ?>


</div>
