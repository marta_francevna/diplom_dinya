<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pages-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label('Заголовок') ?>


    <?= $form->field($model, 'content')->widget(\dosamigos\ckeditor\CKEditor::className(), [
        'options'       => ['rows' => 6],
        'preset'        => 'full',
        'clientOptions' => [
            'filebrowserUploadUrl' => '/admin/upload',
        ],
    ]) ?>

    <?= $form->field($model, 'type_id')->dropDownList([
        '1' => 'Административные процедуры',
        '2' => 'Наши услуги',
        '3' => 'Обращение граждан',
        '4' => 'О нас',
        '5' => 'Требования законодательства',
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
