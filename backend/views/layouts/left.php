<?php
use yii\bootstrap\Nav;

?>
<aside class="left-side sidebar-offcanvas">

    <section class="sidebar">

        <?php if (!Yii::$app->user->isGuest) : ?>
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="<?= $directoryAsset ?>/img/avatar5.png" class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p>Привет, <?= @Yii::$app->user->identity->username ?></p>
                    <a href="<?= $directoryAsset ?>/#">
                        <i class="fa fa-circle text-success"></i> Online
                    </a>
                </div>
            </div>
        <?php endif ?>

       <?php
        $menuItems = [];
        if (!Yii::$app->user->isGuest) {
            $menuItems[] = ['label' => '<span class="fa fa-newspaper-o"></span> Новости', 'url' => ['/news'],  'active' => in_array(\Yii::$app->controller->id, ['news'])];
            $menuItems[] = ['label' => '<span class="fa fa-file"></span> Страницы', 'url' => ['/pages'],  'active' => in_array(\Yii::$app->controller->id, ['pages'])];
            $menuItems[] = ['label' => '<span class="fa fa-envelope-open-o"></span> Записи', 'url' => ['/records'],  'active' => in_array(\Yii::$app->controller->id, ['records'])];
        } ?>
        <?=
        Nav::widget(
            [
                'encodeLabels' => false,
                'options' => ['class' => 'sidebar-menu'],
                'items' => $menuItems
            ]
        );
        ?>



    </section>

</aside>
