<?php

use yii\helpers\Html;

/* @var $this yii\web\View
 * @var common\models\News $model
 */

$this->title = $model->title;
?>
<section class="title">
    <div class="container">
        <div class="row-fluid" style="margin-top: 70px;">
            <div class="span6">
                <h1 style="line-height: 1;"><?= $model->title ?></h1>
            </div>
            <div class="span6">
                <ul class="breadcrumb pull-right">
                    <li><a href="/">Главная</a> <span class="divider">/</span></li>
                    <li><a href="/news">Новости</a> <span class="divider">/</span></li>
                    <li class="active"><?= $model->title ?></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="about-us" class="container">
    <div class="row-fluid">
        <div class="span12">
            <div class="blog">
                <div class="blog-item well">
                    <a href="#"><h2><?= $model->title ?></h2></a>
                    <?= $model->content ?>
                </div>
            </div>
        </div>
    </div>
</section>
