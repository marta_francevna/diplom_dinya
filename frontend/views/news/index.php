<?php

use yii\widgets\ListView;

/* @var $this yii\web\View */

$this->title = 'Новости';
?>
<section class="title">
    <div class="container">
        <div class="row-fluid" style="margin-top: 70px;">
            <div class="span6">
                <h1 style="line-height: 1;">Новости</h1>
            </div>
            <div class="span6">
                <ul class="breadcrumb pull-right">
                    <li><a href="/">Главная</a> <span class="divider">/</span></li>
                    <li class="active">Новости</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="about-us" class="container main">
    <div class="row-fluid">
        <div class="blog">
            <?php foreach ($models as $model) { ?>
                <div class="blog-item well">
                    <a href="/news/<?= $model->slug ?>"><h2><?= \yii\helpers\Html::encode($model->title) ?></h2></a>

                    <p><?= \yii\helpers\HtmlPurifier::process(\yii\helpers\StringHelper::truncate($model->content, 400, '...')) ?></p>
                    <a class="btn btn-link" href="/news/<?= $model->slug ?>">Читать <i class="icon-angle-right"></i></a>
                </div>
            <?php } ?>
        </div>
    </div>
</section>

<?php echo \yii\widgets\LinkPager::widget([
    'pagination' => $pages,
]);
?>

