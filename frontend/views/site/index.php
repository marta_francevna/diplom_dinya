<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Главная | РУП "Витебское агенство государственной регистрации и земельного кадастра"';
?>
<!--Slider-->
<section id="slide-show">
    <div id="slider" class="sl-slider-wrapper">

        <!--Slider Items-->
        <div class="sl-slider">
            <!--Slider Item1-->
            <div class="sl-slide item1" data-orientation="horizontal" data-slice1-rotation="-25"
                 data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                <div class="sl-slide-inner">
                    <div class="container">
                        <img class="pull-right" src="images/sample/slider/img1.png" alt="" />
                        <h2>Интересные идеи</h2>
                        <h3 class="gap">Гарантия сделки</h3>
                        <a class="btn btn-large btn-transparent" href="#">Узнать больше</a>
                    </div>
                </div>
            </div>
            <!--/Slider Item1-->

            <!--Slider Item2-->
            <div class="sl-slide item2" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15"
                 data-slice1-scale="1.5" data-slice2-scale="1.5">
                <div class="sl-slide-inner">
                    <div class="container">
                        <img class="pull-right" src="images/sample/slider/img2.png" alt="" />
                        <h2>План &amp; Анализ</h2>
                        <h3 class="gap">Мы поможем Вам.</h3>
                        <a class="btn btn-large btn-transparent" href="#">Узнать больше</a>
                    </div>
                </div>
            </div>
            <!--Slider Item2-->

            <!--Slider Item3-->
            <div class="sl-slide item3" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3"
                 data-slice1-scale="2" data-slice2-scale="1">
                <div class="sl-slide-inner">
                    <div class="container">
                        <img class="pull-right" src="images/sample/slider/img3.png" alt="" />
                        <h2>Отдел риэлторских услуг</h2>
                        <h3 class="gap">Репутация проверенная временем.</h3>
                        <a class="btn btn-large btn-transparent" href="http://www.gosrielt.by/">Перейти к сайту</a>
                    </div>
                </div>
            </div>
            <!--Slider Item3-->

        </div>
        <!--/Slider Items-->

        <!--Slider Next Prev button-->
        <nav id="nav-arrows" class="nav-arrows">
            <span class="nav-arrow-prev"><i class="icon-angle-left"></i></span>
            <span class="nav-arrow-next"><i class="icon-angle-right"></i></span>
        </nav>
        <!--/Slider Next Prev button-->

    </div>
    <!-- /slider-wrapper -->
</section>
<!--/Slider-->


<!--Services-->
<section id="services">
    <div class="container">
        <div class="center gap">
            <h3>Информация</h3>
            <p class="lead">Мы заботимся о своих клиентах</p>
        </div>

        <div class="row-fluid">
            <div class="span4">
                <div class="media">
                    <div class="pull-left">
                        <i class="icon-globe icon-medium"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Административные процедуры</h4>
                        <?php foreach ($pages as $page) { ?>
                            <?php if ($page->type->title == 'admin_procedure') { ?>
                                <li><a href="/pages/<?= $page->slug ?>"><?= $page->title ?></a></li>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="span4">
                <div class="media">
                    <div class="pull-left">
                        <i class="icon-thumbs-up-alt icon-medium"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Наши услуги</h4>
                        <?php foreach ($pages as $page) { ?>
                            <?php if ($page->type->title == 'our_services') { ?>
                                <li><a href="/pages/<?= $page->slug ?>"><?= $page->title ?></a></li>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="span4">
                <div class="media">
                    <div class="pull-left">
                        <i class="icon-leaf icon-medium icon-rounded"></i>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Обращение граждан</h4>
                        <?php foreach ($pages as $page) { ?>
                            <?php if ($page->type->title == 'appeal_of_citizens') { ?>
                                <li><a href="/pages/<?= $page->slug ?>"><?= $page->title ?></a></li>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>


    </div>
</section>
<!--/Services-->

<section id="recent-works">
    <div class="container">
        <div class="center">
            <h3>Доска почета</h3>
            <p class="lead">За добросовестную и эффективную работу, достижение высоких результатов в производственной
                            деятельности предприятия по итогам трудовой деятельности за 2018 год на Доску почета
                            Республиканского унитарного предприятия «Витебское агентство по государственной регистрации
                            и земельному кадастру» занесены следующие работники:</p>
        </div>
        <div class="gap"></div>
        <ul class="gallery col-4">
            <!--Item 1-->
            <li>
                <div class="preview">
                    <img alt=" " src="images/portfolio/thumb/item1.jpg">
                    <div class="overlay">
                    </div>
                    <div class="links">
                        <a data-toggle="modal" href="#modal-1"><i class="icon-eye-open"></i></a><a href="#"><i
                                    class="icon-link"></i></a>
                    </div>
                </div>
                <div class="desc">
                    <h5>Это главный сотрудник...</h5>
                </div>
                <div id="modal-1" class="modal hide fade">
                    <a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i
                                class="icon-remove"></i></a>
                    <div class="modal-body">
                        <img src="images/portfolio/full/item1.jpg" alt=" " width="100%" style="max-height:400px">
                    </div>
                </div>
            </li>
            <!--/Item 1-->

            <!--Item 2-->
            <li>
                <div class="preview">
                    <img alt=" " src="images/portfolio/thumb/item2.jpg">
                    <div class="overlay">
                    </div>
                    <div class="links">
                        <a data-toggle="modal" href="#modal-1"><i class="icon-eye-open"></i></a><a href="#"><i
                                    class="icon-link"></i></a>
                    </div>
                </div>
                <div class="desc">
                    <h5>Это главный сторудник...</h5>
                </div>
                <div id="modal-1" class="modal hide fade">
                    <a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i
                                class="icon-remove"></i></a>
                    <div class="modal-body">
                        <img src="images/portfolio/full/item2.jpg" alt=" " width="100%" style="max-height:400px">
                    </div>
                </div>
            </li>
            <!--/Item 2-->

            <!--Item 3-->
            <li>
                <div class="preview">
                    <img alt=" " src="images/portfolio/thumb/item3.jpg">
                    <div class="overlay">
                    </div>
                    <div class="links">
                        <a data-toggle="modal" href="#modal-3"><i class="icon-eye-open"></i></a><a href="#"><i
                                    class="icon-link"></i></a>
                    </div>
                </div>
                <div class="desc">
                    <h5>Это главный сотрудник...</h5>
                </div>
                <div id="modal-3" class="modal hide fade">
                    <a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i
                                class="icon-remove"></i></a>
                    <div class="modal-body">
                        <img src="images/portfolio/full/item3.jpg" alt=" " width="100%" style="max-height:400px">
                    </div>
                </div>
            </li>
            <!--/Item 3-->

            <!--Item 4-->
            <li>
                <div class="preview">
                    <img alt=" " src="images/portfolio/thumb/item4.jpg">
                    <div class="overlay">
                    </div>
                    <div class="links">
                        <a data-toggle="modal" href="#modal-4"><i class="icon-eye-open"></i></a><a href="#"><i
                                    class="icon-link"></i></a>
                    </div>
                </div>
                <div class="desc">
                    <h5>Это главный сотрудник</h5>
                </div>
                <div id="modal-4" class="modal hide fade">
                    <a class="close-modal" href="javascript:;" data-dismiss="modal" aria-hidden="true"><i
                                class="icon-remove"></i></a>
                    <div class="modal-body">
                        <img src="images/portfolio/full/item4.jpg" alt=" " width="100%" style="max-height:400px">
                    </div>
                </div>
            </li>
            <!--/Item 4-->

        </ul>
    </div>

</section>

<section id="clients" class="main">
    <div class="container">
        <div class="row-fluid">
            <div class="span2">
                <div class="clearfix">
                    <h4 class="pull-left">Наши партнеры</h4>
                    <div class="pull-right">
                        <a class="prev" href="#myCarousel" data-slide="prev"><i class="icon-angle-left icon-large"></i></a>
                        <a class="next" href="#myCarousel" data-slide="next"><i class="icon-angle-right icon-large"></i></a>
                    </div>
                </div>
                <p>Это информация может Вас заинтересовать.</p>
            </div>
            <div class="span10">
                <div id="myCarousel" class="carousel slide clients">
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="active item">
                            <div class="row-fluid">
                                <ul class="thumbnails">
                                    <li class="span3"><a href="http://vl.nca.by/"><img
                                                    src="images/sample/clients/client1.png"></a></li>
                                    <li class="span3"><a href="http://www.gki.gov.by/ru/"><img
                                                    src="images/sample/clients/client2.png"></a></li>
                                    <li class="span3"><a href="http://www.president.gov.by/"><img
                                                    src="images/sample/clients/client3.png"></a></li>
                                    <li class="span3"><a href="http://www.pravo.by/"><img
                                                    src="images/sample/clients/client4.png"></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="item">
                            <div class="row-fluid">
                                <ul class="thumbnails">
                                    <li class="span3"><a href="#"><img src="images/sample/clients/client4.png"></a></li>
                                    <li class="span3"><a href="#"><img src="images/sample/clients/client3.png"></a></li>
                                    <li class="span3"><a href="#"><img src="images/sample/clients/client2.png"></a></li>
                                    <li class="span3"><a href="#"><img src="images/sample/clients/client1.png"></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="item">
                            <div class="row-fluid">
                                <ul class="thumbnails">
                                    <li class="span3"><a href="#"><img src="images/sample/clients/client1.png"></a></li>
                                    <li class="span3"><a href="#"><img src="images/sample/clients/client2.png"></a></li>
                                    <li class="span3"><a href="#"><img src="images/sample/clients/client3.png"></a></li>
                                    <li class="span3"><a href="#"><img src="images/sample/clients/client4.png"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /Carousel items -->

                </div>
            </div>
        </div>
    </div>
</section>


