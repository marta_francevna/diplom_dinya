<?php

/* @var $this yii\web\View */

$this->title = 'Запись';

?>

<section class="title">
    <div class="container">
        <div class="row-fluid" style="margin-top: 70px;">
            <div class="span6">
                <h1 style="line-height: 1;"><?= $model->type == 1 ? 'Запись физического лица' : 'Запись юридического лица' ?></h1>
            </div>
            <div class="span6">
                <ul class="breadcrumb pull-right">
                    <li><a href="/">Главная</a> <span class="divider">/</span></li>
                    <li class="active"><?= $model->type == 1 ? 'Запись физического лица' : 'Запись юридического лица' ?></li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section id="contact-page" class="container">
    <div class="row-fluid">
        <?php $form = \yii\widgets\ActiveForm::begin([]) ?>
        <?php if (Yii::$app->session->hasFlash('success')): ?>
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?= Yii::$app->session->getFlash('success') ?>
            </div>
        <?php endif; ?>
        <div class="row-fluid">
            <div class="span5">
                <?= $form->field($model, 'full_name')->textInput(['style' => 'width: 65%;', 'placeholder' => 'ФИО'])->label('ФИО'); ?>
                <?= $form->field($model, 'email')->textInput(['style' => 'width: 65%;', 'placeholder' => 'Email'])->label('Email'); ?>
                <label>Дата записи</label>
                <input type="text" id="checkin" name='RecordForm[date_record]' class="salsa-calendar-input form-control"
                       autocomplete="off" value="" placeholder="Дата записи">
            </div>

            <div class="span7">
                <?= $form->field($model, 'comment')->textarea(['rows' => 8, 'style' => 'width: 50%;', 'placeholder' => 'Комментарий'])->label('Комментарий') ?>
            </div>

            <?= $form->field($model, 'type')->hiddenInput()->label('') ?>
        </div>
        <button type="submit" class="btn btn-primary btn-large pull-right" style="float: left;">Отправить</button>
        <p></p>
    </div>
</section>

<?php \yii\widgets\ActiveForm::end() ?>

