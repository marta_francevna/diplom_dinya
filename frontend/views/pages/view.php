<?php
use yii\helpers\Html;
/* @var $this yii\web\View
 * @var common\models\Pages $model
*/


$this->title = $model->title;
?>
<section class="title">
    <div class="container">
        <div class="row-fluid" style="margin-top: 70px;">
            <div class="span6">
                <h1 style="line-height: 1;"><?= $model->title ?></h1>
            </div>
            <div class="span6">
                <ul class="breadcrumb pull-right">
                    <li><a href="/">Главная</a> <span class="divider">/</span></li>
                    <li class="active"><?= $model->title ?></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- / .title -->

<!-- Privacy-policy -->
<section id="privacy-policy" class="container">
    <?= $model->content ?>
</section>

