<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
$pages =  \common\models\Pages::find()->with('type')->all();
AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="/img/favicon.ico"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody();

?>

<!--Header-->
<header class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a id="logo" class="pull-left" href="/"></a>
            <div class="nav-collapse collapse pull-right">
                <ul class="nav">

                    <li class="active">
                        <a href="/">Главная</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">О нас <i class="icon-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <?php foreach ($pages as $page) { ?>
                                <?php if ($page->type->title == 'about') { ?>
                                    <li><a href="/pages/<?= $page->slug ?>"><?= $page->title ?></a></li>
                                <?php }  ?>
                            <?php } ?>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Требования законодательства <i class="icon-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <?php foreach ($pages as $page) { ?>
                                <?php if ($page->type->title == 'legal_requirements') { ?>
                                    <li><a href="/pages/<?= $page->slug ?>"><?= $page->title ?></a></li>
                                <?php }  ?>
                            <?php } ?>
                        </ul>
                    <li><a href="/news">Новости</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Предварительная запись <i class="icon-angle-down"></i></a>
                        <ul class="dropdown-menu">
                            <li><a href="/record/1">Предварительная запись физических лиц</a></li>
                            <li><a href="/record/2">Предварительна запись юридических лиц</a></li>
                        </ul>
                    </li>
                    <li class="login">
                        <a data-toggle="modal" href="http://admin.diplom.loc"><i class="icon-lock"></i></a>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</header>
<!-- /header -->
<?= $content ?>
<!--Bottom-->
<section id="bottom" class="main">
    <!--Container-->
    <div class="container">

        <!--row-fluids-->
        <div class="row-fluid">

            <!--Contact Form-->
            <div class="span3">
                <h4>АДРЕС</h4>
                <ul class="unstyled address">
                    <li>
                        <i class="icon-home"></i><strong>Адрес:</strong> ул. Политехническая, 5/18<br>
                    </li>
                    <li>
                        <i class="icon-envelope"></i>
                        <strong>Email: </strong> d200@nca.by
                    </li>
                    <li>
                        <i class="icon-globe"></i>
                        <strong>Веб-сайт:</strong>
                    </li>
                    <li>
                        <i class="icon-phone"></i>
                        <strong>Телефон для справок:</strong> 8(212) 60-91-30
                    </li>
                </ul>
            </div>
            <!--End Contact Form-->

            <!--Important Links-->
            <div id="tweets" class="span3">
                <h4>О нас</h4>
                <div>
                    <ul class="arrow">
                        <?php foreach ($pages as $page) { ?>
                            <?php if ($page->type->title == 'about') { ?>
                                <li><a href="/pages/<?= $page->slug ?>"><?= $page->title ?></a></li>
                            <?php }  ?>
                        <?php } ?>
                        <li><a href="/news">Новости</a></li>
                    </ul>
                </div>
            </div>
            <!--Important Links-->
            <div class="span3">
                <h4>Альбом предприятия</h4>
                <div class="row-fluid first">
                    <ul class="thumbnails">
                        <li class="span3">
                            <a href="http://www.flickr.com/photos/76029035@N02/6829540293/"
                               title="01 (254) by Victor1558, on Flickr"><img
                                        src="http://farm8.staticflickr.com/7003/6829540293_bd99363818_s.jpg" width="75"
                                        height="75" alt="01 (254)"></a>
                        </li>
                        <li class="span3">
                            <a href="http://www.flickr.com/photos/76029035@N02/6829537417/"
                               title="01 (196) by Victor1558, on Flickr"><img
                                        src="http://farm8.staticflickr.com/7013/6829537417_465d28e1db_s.jpg" width="75"
                                        height="75" alt="01 (196)"></a>
                        </li>
                        <li class="span3">
                            <a href="http://www.flickr.com/photos/76029035@N02/6829527437/"
                               title="01 (65) by Victor1558, on Flickr"><img
                                        src="http://farm8.staticflickr.com/7021/6829527437_88364c7ec4_s.jpg" width="75"
                                        height="75" alt="01 (65)"></a>
                        </li>
                        <li class="span3">
                            <a href="http://www.flickr.com/photos/76029035@N02/6829524451/"
                               title="01 (6) by Victor1558, on Flickr"><img
                                        src="http://farm8.staticflickr.com/7148/6829524451_a725793358_s.jpg" width="75"
                                        height="75" alt="01 (6)"></a>
                        </li>
                    </ul>
                </div>
                <div class="row-fluid">
                    <ul class="thumbnails">
                        <li class="span3">
                            <a href="http://www.flickr.com/photos/76029035@N02/6829524451/"
                               title="01 (6) by Victor1558, on Flickr"><img
                                        src="http://farm8.staticflickr.com/7148/6829524451_a725793358_s.jpg" width="75"
                                        height="75" alt="01 (6)"></a>
                        </li>
                        <li class="span3">
                            <a href="http://www.flickr.com/photos/76029035@N02/6829540293/"
                               title="01 (254) by Victor1558, on Flickr"><img
                                        src="http://farm8.staticflickr.com/7003/6829540293_bd99363818_s.jpg" width="75"
                                        height="75" alt="01 (254)"></a>
                        </li>
                        <li class="span3">
                            <a href="http://www.flickr.com/photos/76029035@N02/6829537417/"
                               title="01 (196) by Victor1558, on Flickr"><img
                                        src="http://farm8.staticflickr.com/7013/6829537417_465d28e1db_s.jpg" width="75"
                                        height="75" alt="01 (196)"></a>
                        </li>
                        <li class="span3">
                            <a href="http://www.flickr.com/photos/76029035@N02/6829527437/"
                               title="01 (65) by Victor1558, on Flickr"><img
                                        src="http://farm8.staticflickr.com/7021/6829527437_88364c7ec4_s.jpg" width="75"
                                        height="75" alt="01 (65)"></a>
                        </li>
                    </ul>
                </div>

            </div>

        </div>
        <!--/row-fluid-->
    </div>
    <!--/container-->

</section>
<!--/bottom-->
<!--Footer-->
<footer id="footer">
    <div class="container">
        <div class="row-fluid">
            <div class="span5 cp">
                &copy; 2019 <a target="_blank" href="http://www.vagr.by/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">Оригинальная версия</a>. Дипломный про.
            </div>
            <div class="span1">
                <a id="gototop" class="gototop pull-right" href="#"><i class="icon-angle-up"></i></a>
            </div>
            <!--/Goto Top-->
        </div>
    </div>
</footer>
<!--/Footer-->

<?php $this->endBody() ?>
<!-- SL Slider -->
<script type="text/javascript">
    $(function() {
        var Page = (function() {

            var $navArrows = $( '#nav-arrows' ),
                slitslider = $( '#slider' ).slitslider( {
                    autoplay : true
                } ),

                init = function() {
                    initEvents();
                },
                initEvents = function() {
                    $navArrows.children( ':last' ).on( 'click', function() {
                        slitslider.next();
                        return false;
                    });

                    $navArrows.children( ':first' ).on( 'click', function() {
                        slitslider.previous();
                        return false;
                    });
                };

            return { init : init };

        })();

        Page.init();
    });
</script>
<!-- /SL Slider -->
</body>
</html>
<?php $this->endPage() ?>
