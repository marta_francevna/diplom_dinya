<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/bootstrap-responsive.min.css',
        'css/font-awesome.min.css',
        'css/main.css',
        'css/sl-slide.css',
        'css/SalsaCalendar.min.css'
    ];


    public $js = [
        'js/vendor/modernizr-2.6.2-respond-1.1.0.min.js',
        'js/vendor/jquery-1.9.1.min.js',
        'js/SalsaCalendar.min.js',
        'js/vendor/bootstrap.min.js',
        'js/main.js',
        'js/jquery.ba-cond.min.js',
        'js/jquery.slitslider.js',
    ];
}
