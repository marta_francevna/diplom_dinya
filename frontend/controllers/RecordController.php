<?php

namespace frontend\controllers;

use common\forms\RecordForm;
use common\models\Orders;
use common\models\Platforms;
use common\models\Records;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

/**
 * Record controller
 */
class RecordController extends Controller
{

    public function actionIndex($id)
    {
        $model = new RecordForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $order = new Records();
            $order->attributes = Yii::$app->request->post('RecordForm');
            $dateValue = Yii::$app->request->post('RecordForm')['date_record'];
            $timestamp = \DateTime::createFromFormat('d/m/Y', $dateValue)->getTimestamp();
            $order->date_record = date('Y-m-d H:i:s',$timestamp);
            if ($order->validate()) {
                $order->save(false);
                Yii::$app->getSession()->setFlash('success', 'Отправлено');

                return $this->render('index', ['model' => $model,]);
            }

        } else {
            $model->type = $id;

            return $this->render('index', ['model' => $model]);
        }
    }



}
