<?php

namespace frontend\controllers;

use common\models\Articles;
use common\models\News;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\web\Controller;

/**
 * News controller
 */
class NewsController extends Controller
{

    public function actionIndex()
    {
        $query = News::find();
        $countQuery = clone $query;

        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize' => 10]);

        $pages->pageSizeParam = false;
        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'models' => $models,
            'pages' => $pages,
            ]);
    }


    public function actionView($slug)
    {
        $model = News::findOne(['slug' => $slug]);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

}
