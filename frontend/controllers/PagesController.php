<?php
namespace frontend\controllers;

use common\models\Pages;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

/**
 * Pages controller
 */
class PagesController extends Controller
{

    public function actionView($slug)
    {
	    $model = Pages::findOne(['slug' => $slug]);
        return $this->render('view', [
            'model' => $model
        ]);
    }

}
